<?php


/**
 * Creates nodes from feed items.
 */
class BOMObservationProcessor extends FeedsProcessor {
  
  /**
   * Implementation of FeedsProcessor::process().
   */
  public function process(FeedsImportBatch $batch, FeedsSource $source) {
    
    // Keep track of processed items in this pass, set total number of items.
    $processed = 0;
    if (!$batch->getTotal(FEEDS_PROCESSING)) {
      $batch->setTotal(FEEDS_PROCESSING, count($batch->items));
    }
    
    while ($item = $batch->shiftItem()) {
      
      $site = $item['site'];
      $locations = bom_location_fetch_by_site($site);
      
      foreach($locations as $location) {
        
        $nid = $location['nid'];
        
        bom_location_delete_observation($nid);
        
        $observation = array(
          'nid' => $nid,
          'issued' => $item['issued'],
          'data' => $item['data'],
        );
        
        bom_location_insert_observation($observation);
        $batch->updated++;
      }
    }

    // Set messages.
    if ($batch->updated) {
      drupal_set_message(format_plural($updated, 'Updated @number item.', 'Updated @number items.', array('@number' => $batch->updated)));
    }
    else {
      drupal_set_message(t('There are no new items.'));
    }

  }

  /**
   * Implementation of FeedsProcessor::clear().
   */
  public function clear(FeedsBatch $batch, FeedsSource $source) {

  }
  
}