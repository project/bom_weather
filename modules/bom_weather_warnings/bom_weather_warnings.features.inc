<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function bom_weather_warnings_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => 1);
  }
  elseif ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function bom_weather_warnings_node_info() {
  $items = array(
    'bom_warning' => array(
      'name' => t('BOM Weather Warning'),
      'module' => 'features',
      'description' => t('Shows BOM weather warnings populated by an RSS feed.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function bom_weather_warnings_views_api() {
  return array(
    'api' => '2',
  );
}
