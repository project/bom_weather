<?php

/**
 * Implementation of hook_strongarm().
 */
function bom_weather_warnings_strongarm() {
  $export = array();

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_bom_warning';
  $strongarm->value = array(
    'title' => '-5',
    'revision_information' => '1',
    'author' => '0',
    'options' => '2',
    'menu' => '-3',
    'path' => '5',
    'attachments' => '6',
    'custom_breadcrumbs' => '4',
    'print' => '3',
    'workflow' => '-2',
    'nodewords' => '-1',
  );
  $export['content_extra_weights_bom_warning'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_bom_warning';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
  );
  $export['node_options_bom_warning'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'upload_bom_warning';
  $strongarm->value = '0';
  $export['upload_bom_warning'] = $strongarm;

  return $export;
}
