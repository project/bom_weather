<?php

/**
 * Implementation of hook_feeds_importer_default().
 */
function bom_weather_feeds_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'bom_forecasts';
  $feeds_importer->config = array(
    'name' => 'BOM Forecasts',
    'description' => 'BOM Weather forecast',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'BOMForecastParser',
      'config' => array(),
    ),
    'processor' => array(
      'plugin_key' => 'BOMForecastProcessor',
      'config' => array(
        'mappings' => array(),
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => 1800,
    'expire_period' => 3600,
    'import_on_create' => TRUE,
  );
  $export['bom_forecasts'] = $feeds_importer;

  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'bom_locations';
  $feeds_importer->config = array(
    'name' => 'BOM Locations',
    'description' => 'BOM weather locations',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'direct' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'content_type' => 'bom_location',
        'input_format' => '0',
        'update_existing' => '0',
        'expire' => '-1',
        'mappings' => array(
          0 => array(
            'source' => 'Name',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'Type',
            'target' => 'area_type',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'AAC',
            'target' => 'aac',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'PAAC',
            'target' => 'paac',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'Site',
            'target' => 'site',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'AAC',
            'target' => 'guid',
            'unique' => 1,
          ),
          6 => array(
            'source' => 'Radar',
            'target' => 'radar',
            'unique' => FALSE,
          ),
        ),
        'author' => 0,
        'authorize' => 0,
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => 1800,
    'expire_period' => 3600,
    'import_on_create' => TRUE,
  );
  $export['bom_locations'] = $feeds_importer;

  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'bom_observations';
  $feeds_importer->config = array(
    'name' => 'BOM Observations',
    'description' => 'BOM Weather Observations',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'BOMObservationParser',
      'config' => array(),
    ),
    'processor' => array(
      'plugin_key' => 'BOMObservationProcessor',
      'config' => array(
        'mappings' => array(),
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => 1800,
    'expire_period' => 3600,
    'import_on_create' => TRUE,
  );
  $export['bom_observations'] = $feeds_importer;

  return $export;
}
