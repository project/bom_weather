<?php

/**
 * Implementation of hook_strongarm().
 */
function bom_weather_feeds_strongarm() {
  $export = array();

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_bom_location_pattern';
  $strongarm->value = 'weather/[title-raw]';
  $export['pathauto_node_bom_location_pattern'] = $strongarm;

  return $export;
}
