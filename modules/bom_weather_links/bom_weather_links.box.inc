<?php

/**
 * Implementation of hook_default_box().
 */
function bom_weather_links_default_box() {
  $export = array();

  $box = new stdClass;
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'bom_weather_more_weather';
  $box->plugin_key = 'simple';
  $box->title = 'More Weather';
  $box->description = 'BOM More Weather';
  $box->options = array(
    'body' => '<ul><li><a href="http://www.bom.gov.au/australia/satellite/index.shtml" target="_blank">Satellite Images</a></li><li><a href="http://www.bom.gov.au/australia/charts/synoptic_col.shtml" target="_blank">Synoptic Maps</a></li><li><a href="http://www.bom.gov.au/weather/radar/" target="_blank">Rain Radars</a></li></ul>',
    'format' => '2',
  );
  $export['bom_weather_more_weather'] = $box;

  return $export;
}
