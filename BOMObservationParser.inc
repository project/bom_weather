<?php

/**
 * A parser for Bureau of Meterology forecast data
 */
class BOMObservationParser extends FeedsParser {
  
  /**
   * Implementation of FeedsParser::parse().
   */
  public function parse(FeedsImportBatch $batch, FeedsSource $source) {
    // Set time zone to GMT for parsing dates with strtotime().
    $tz = date_default_timezone_get();
    date_default_timezone_set('GMT');
    
    $decoded_json = json_decode($batch->getRaw(), TRUE);
    
    $header = $decoded_json['observations']['header'];
    
    // grab the top-most as we only want the latest observations
    $data = $decoded_json['observations']['data'][0];
    
    if (isset($data)) {
      $site = $data['wmo']; // site id
    
      $observation['site'] = $site;
      $observation['issued'] = strtotime($data['aifstime_utc']);
      $observation['data'] = $data;
      if (isset($header['refresh_message'])) {
        $observation['data']['refresh_message'] = $header['refresh_message'];
      }
      $batch->title = 'BOM Weather Observations for ('. $data['wmo'] .')';
    
      $batch->items[] = $observation;
    }
    date_default_timezone_set($tz);
  }

}
