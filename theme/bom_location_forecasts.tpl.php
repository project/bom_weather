<div class="forecasts forecast-days-<?php print count($location_forecasts)?>">
<h2><?php print count($location_forecasts)?>-Day Forecast</h2>
<?php foreach ($location_forecasts as $forecast): ?>
  <div class="forecast">
    <span class="date"><strong><?php print date('D', $forecast['period_start']); ?></strong></span>
    <span class="icon">Icon: <?php print $forecast['data']['forecast_icon_code'] ?></span>
    <span class="min-temp">Min: <?php print $forecast['data']['air_temperature_minimum'] ?></span>
    <span class="max-temp">Max: <?php print $forecast['data']['air_temperature_maximum'] ?></span>
    <span class="precis"><?php print $forecast['data']['precis'] ?></span>
  </div>
<?php endforeach; ?>
</div>