<div class="bom-nearby-locations">
<h2><?php print $district['title'] ?></h2>
<?php foreach($location_summaries as $location): ?>
  <div class="location">
    <?php print l($location['title'], 'node/'. $location['nid']) ?>
    <span class="max-temp">Icon: <?php print $location['todays_forecast']['forecast_icon_code'] ?></span>
    <span class="current-temp">Current Temp: <?php print $location['observation']['air_temp'] ?></span>
    <span class="max-temp">Max Temp: <?php print $location['todays_forecast']['air_temperature_maximum'] ?></span>
  </div>
<?php endforeach ?>
</div>