<div class="bom-summary">
  <span class="name"><?php print $location->title; ?></span>
  <a href="#" class="bom-change">change</a>
  <span class="icon"><?php print $todays_forecast['forecast_icon_code'] ?></span>
  <span class="current-temp"><?php print $observation['air_temp'] ?></span>
  <span class="max-temp">Max <?php print $todays_forecast['air_temperature_maximum'] ?></span>
  <?php print l('more', 'node/'. $location->nid) ?>
</div>