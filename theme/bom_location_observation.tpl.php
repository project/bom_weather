<div class="todays-forecast">
<h3>Today's Weather</h3>
<span class="date"><strong><?php print date('D', $todays_forecast['period_start']); ?></strong></span>
<span class="icon">Icon: <?php print $todays_forecast['data']['todays_forecast_icon_code'] ?></span>
<span class="min-temp">Min: <?php print $todays_forecast['data']['air_temperature_minimum'] ?></span>
<span class="max-temp">Max: <?php print $todays_forecast['data']['air_temperature_maximum'] ?></span>
<span class="precis"><?php print $todays_forecast['data']['precis'] ?></span>
</div>
<div class="bom-observations">
<h3>Observations</h3>
<span class="issue-date">Issued: <?php print  date('Y-m-d H:i:s', $observation['issued']); ?></span>
<?php if (isset($observation['data'])): ?>
  <ul>
  <?php foreach ($observation['data'] as $key => $value): ?>
    <li><strong><?php print $key ?>:</strong><?php print $value ?></li>
  <?php endforeach; ?>
  </ul>
<?php endif ?>
</div>