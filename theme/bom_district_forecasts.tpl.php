<div class="district">
<h2><?php print $district['title']; ?></h2>
<div class="district-forecasts">
<?php foreach ($district_forecasts as $forecast): ?>
  <div class="forecast">
    <span class="date"><strong><?php print date('D', $forecast['period_start']); ?></strong></span>
    <span class="forecast">Forecast: <?php print $forecast['data']['forecast'] ?></span>
  </div>
<?php endforeach; ?>
</div>
</div>