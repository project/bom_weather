<div class="bom-region">
<h2><?php print $region['title']; ?></h2>
<div class="district-forecasts">
<?php foreach ($region_forecasts as $forecast): ?>
  <div class="forecast">
    <span class="date"><strong><?php print date('l jS F', $forecast['period_start']); ?></strong></span>
    <span class="forecast">Forecast: <?php print $forecast['data']['synopsis'] ?></span>
  </div>
<?php endforeach; ?>
</div>
</div>