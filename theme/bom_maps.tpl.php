<div class="bom-maps">
  <div class="radar">
    <img src="http://www.bom.gov.au/radar/<?php print $radar;?>.gif" alt="Radar" />
  </div>
  <div class="satellite">
    <img src="http://www.bom.gov.au/gms/IDE00135.<?php print $satellite;?>.jpg" alt="Satellite" />
    <p>Infrared image courtesy of the Japan Meteorological Agency. Blue Marble surface image courtesy of NASA.</p>
  </div>
  <div class="synoptic">
    <img src="http://www.bom.gov.au/fwo/IDY00030.<?php print $synoptic;?>.png" alt="Synoptic" />
  </div>
</div>