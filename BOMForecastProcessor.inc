<?php

define('BOM_SYDNEY_MET_AAC', 'NSW_ME001');
define('BOM_SYDNEY_DIST_AAC', 'NSW_PW005');
define('BOM_NSW_AAC', 'NSW_FA001');

/**
 * Creates nodes from feed items.
 */
class BOMForecastProcessor extends FeedsProcessor {
  
  /**
   * Implementation of FeedsProcessor::process().
   */
  public function process(FeedsImportBatch $batch, FeedsSource $source) {
    
    // Keep track of processed items in this pass, set total number of items.
    $processed = 0;
    if (!$batch->getTotal(FEEDS_PROCESSING)) {
      $batch->setTotal(FEEDS_PROCESSING, count($batch->items));
    }
    
    while ($item = $batch->shiftItem()) {

      $aac = $item['aac'];
      
      // hack because sydney has two product IDs
      if ($aac == BOM_SYDNEY_MET_AAC) {
        $aac = BOM_SYDNEY_DIST_AAC;
      }
      
      $location = bom_location_fetch_by_aac($aac);
      
      if ($location && !empty($item['periods'])) {
        
        $nid = $location['nid'];
        
        $issued = $item['issued'];
        $type = $item['type'];
        
        $forecasts = array();
        foreach ($item['periods'] as $period) {
          
          $day = $period['day'];
          $data = $period['data'];
          $forecast = array(
            'nid' => $nid,
            'day' => $day,
            'issued' => $issued,
            'period_start' => $period['period_start'],
            'period_end' => $period['period_end'],
            'data' => $data,
          );
          
          if ($type == 'region') {
            // only save a region forecast if it has a synopsis
            if (isset($data['synopsis'])) {
              $forecasts[] = $forecast;
            }
          } else {
            $forecasts[] = $forecast;            
          }
        }
        
        
        // handle region forecasts which may not actually contain a forecast
        if (!empty($forecasts)) {
          bom_location_delete_forecasts($nid);
          foreach($forecasts as $f) {
            bom_location_insert_forecast($f);
            $batch->updated++;
          }
        }        
      }
    }
    
    // Set messages.
    if ($batch->updated) {
      drupal_set_message(format_plural($updated, 'Updated @number item.', 'Updated @number items.', array('@number' => $batch->updated)));
    } 
    else {
      drupal_set_message(t('There are no new items.'));
    }
    
  }
  
  /**
   * Implementation of FeedsProcessor::clear().
   */
  public function clear(FeedsBatch $batch, FeedsSource $source) {
    
  }
  
}