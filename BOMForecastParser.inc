<?php

/**
 * A parser for Bureau of Meterology forecast data
 */
class BOMForecastParser extends FeedsParser {
  
  /**
   * Implementation of FeedsParser::parse().
   */
  public function parse(FeedsImportBatch $batch, FeedsSource $source) {
    // Set time zone to GMT for parsing dates with strtotime().
    $tz = date_default_timezone_get();
    date_default_timezone_set('GMT');
    // Yes, using a DOM parser is a bit inefficient, but will do for now
    $xml = new SimpleXMLElement($batch->getRaw());
    
    // grab the issue date
    $issued = strtotime($xml->amoc->{'issue-time-utc'});
    
    foreach ($xml->forecast->area as $area) {
    
      // parse the area info
      $forecast_area = array(
        'aac' => (string) $area['aac'],
        'description' => (string) $area['description'],
        'type' => (string) $area['type'],
        'parent_aac' => (string) $area['parent-aac'],
        'issued' => $issued,
      );
      
      foreach ($area->{'forecast-period'} as $forecast_period) {
        
        $day = (int) $forecast_period['index'];
        // parse the forecast period info
        $period = array(
          'day' => $day,
          'period_start' => strtotime((string)$forecast_period['start-time-utc']),
          'period_end' => strtotime((string)$forecast_period['end-time-utc']),
        );
        
        // generic 'elements' contain some of the numeric data
        foreach ($forecast_period->element as $elem) {
          switch((string) $elem['type']) {
            case 'forecast_icon_code':
              $period['data']['forecast_icon_code'] = (string) $elem;
              break;
            case 'air_temperature_minimum':
              $period['data']['air_temperature_minimum'] = (string) $elem;
              break;
            case 'air_temperature_maximum':
              $period['data']['air_temperature_maximum'] = (string) $elem;
              break;
            default:
              watchdog('BOMForecastParser', 'Unknown element type !type', array('!type' => (string) $elem['type']), WATCHDOG_NOTICE);
          }
        }
        
        // text nodes contain precis
        foreach ($forecast_period->text as $text) {
          switch((string) $text['type']) {
            case 'precis':
              $period['data']['precis'] = (string) $text;
              break;
            case 'synoptic_situation':
              $period['data']['synopsis'] = (string) $text->asXML();
              break;
            case 'forecast':
              $period['data']['forecast'] = (string) $text;
              break;
            default:
              watchdog('BOMForecastParser', 'Unknown text type !type', array('!type' => (string) $text['type']), WATCHDOG_NOTICE);
          }
        }
        
        $forecast_area['periods'][] = $period;
      }
      
      $batch->items[] = $forecast_area;
    }
    date_default_timezone_set($tz);
  }
  
  public function getMappingSources() {
    return array(
      'aac' => array(
        'name' => t('ACC ID'),
        'description' => t('The unique AAC ID'),
      ),
    );
  }
  
}